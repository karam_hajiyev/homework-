package az.iba_tech;

import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Random rand = new Random();
        System.out.println("Enter your name:");
        String name = sc.next();
        System.out.println("Let the game begin " + name);
        int arr[] = new int[20];
        int i = 0;
        int generateVal = rand.nextInt(100);
        while (true) {
            System.out.println("Enter the number:");
            int input = sc.nextInt();
            arr[i] = input;
            if (generateVal == input) {
                System.out.println("Congratulations, " + name);
                System.out.println("Your inputs = ");
                for (int j = 0; j < arr.length; j++) {
                    System.out.print(arr[j] + " ");
                }
                break;
            } else if (generateVal < input) {
                System.out.println("Your number is too big. Please try again.");
            } else {
                System.out.println("Your number is too small. Please try again.");
            }
            i++;
        }

    }


}

